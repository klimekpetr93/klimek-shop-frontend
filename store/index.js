export const state = () => ({
  customers: [],
  currentCustomer: {},
  currentContract: {}
});

export const mutations = {
  SET_CUSTOMERS(state, customers) {
    state.customers = customers;
  },

  SET_CURRENT_CUSTOMER_COMPLET(state, customer) {
    state.currentCustomer = customer;
  },
  SET_CURRENT_CUSTOMER_CARS(state, cars) {
    state.currentCustomer.cars = cars;
  },
  SET_CURRENT_CUSTOMER_PAYMENTS(state, payments) {
    state.currentCustomer.payments = payments;
  },
  SET_CURRENT_CUSTOMER_CONTRACTS(state, contracts) {
    state.currentCustomer = contracts;
  },
  SET_CURRENT_CONTRACT(state, contract) {
    state.currentContract = contract;
  },

  DELETE_CURRENT(state, film_id) {
    state.customers.splice(
      state.customers
        .map(film => {
          return film._id;
        })
        .indexOf(film_id),
      1
    );
  }
};
